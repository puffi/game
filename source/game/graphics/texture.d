module game.graphics.texture;

import gfx.d.types;

struct Texture
{
	enum Type
	{
		_2d,
		_2dArray
	}
	Type type;
	uint width;
	uint height;
	uint depth;
	TextureHandle handle;
}

struct Sampler
{
	Filter min;
	Filter mag;
	Wrap x;
	Wrap y;
	Wrap z;
}