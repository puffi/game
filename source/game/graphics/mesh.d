module game.graphics.mesh;

import gfx.d.types;

struct Mesh
{
	VertexBufferHandle vb;
	IndexBufferHandle ib;
}