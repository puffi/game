module game.graphics.renderstate;

import gfx.d.types;

struct RenderState
{
	VertexShaderHandle vs;
	FragmentShaderHandle fs;
	BlendState blend;
	DepthState depth;
	CullState cull;
	ScissorState scissor;
	TextureHandle[8] textures;
	SamplerHandle[8] samplers;
	UniformBufferHandle[6] uniforms;
	StorageBufferHandle[6] storages;
}