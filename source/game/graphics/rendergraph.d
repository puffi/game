module game.graphics.rendergraph;

import gfx.d.types;

struct RenderGraph
{
	RenderPass* root;
}

struct RenderPass
{
	RenderTargetHandle readTarget;
	RenderTargetHandle writeTarget;

	RTClearValue[Attachment.max+1] clearValues;
	bool useCustomClearValues;

	uint startLayer;
	uint endLayer; // exclusive, startLayer..endLayer
}