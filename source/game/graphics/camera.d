module game.graphics.camera;

import math;
import math.aliases;

struct Camera
{
	enum ProjectionType
	{
		perspective,
		orthographic,
		orthographicPP
	}
	ProjectionType type;
	union
	{
		float yfov;
		float verticalSize;
	}
	float near = 0.1f;
	float far = 100.0f;

	static struct Viewport
	{
		float x = 0;
		float y = 0;
		float w = 1;
		float h = 1;
	}
	Viewport vp;
}