module game.graphics.material;

import game.graphics.renderstate;
import gfx.d.api;
import gfx.d.types;
import game.graphics.texture;

interface IMaterial
{
	IMaterialInstance createInstance();
	IMaterialInstance getInstance(uint index);
	void destroyInstance(uint index);
	const(RenderState)* getDefaultRenderState();
	// const(RenderState)* getRenderState(in PerInstanceState* pis);
}

interface IMaterialInstance
{
	IMaterial getMaterial();
	uint getIndex();
	const(RenderState)* getRenderState();
}

class MaterialTemplate(alias MTD) : IMaterial if(is(typeof(MTD) == MaterialTemplateDescription!T, T...))
{
	alias description = MTD;

	private
	{
		UniformBufferHandle[MTD.uniforms.length] mUniforms;
		StorageBufferHandle[MTD.storages.length] mStorages;

		enum MaxUniformInstances = MaxInstances!(MTD.uniforms);

		mixin(UniformData!(MTD.uniforms));
		mixin(StorageData!(MTD.storages));
		bool[MTD.uniforms.length] mUniformsOutOfDate;
		bool[MTD.storages.length] mStoragesOutOfDate;

		MaterialInstanceTemplate!MTD[MaxUniformInstances] mInstances;

		mixin(UpdateUniformBuffers!(MTD.uniforms));
		mixin(UpdateStorageBuffers!(MTD.storages));

		RenderState mDefaultState;
		RenderState[MaxUniformInstances] mInstanceStates;
	}

	this()
	{
		foreach(i, uniform; MTD.uniforms)
		{
			mUniforms[i] = Gfx.createUniformBuffer(PaddedSize!(uniform.type) * MaxUniformInstances);
		}
		foreach(i, storage; MTD.storages)
		{
			mStorages[i] = Gfx.createStorageBuffer(PaddedSize!(storage.type) * MaxUniformInstances);
		}

		mDefaultState.vs = Gfx.createVertexShader(cast(ubyte[])MTD.vs);
		mDefaultState.fs = Gfx.createFragmentShader(cast(ubyte[])MTD.fs);

		mDefaultState.blend = MTD.defaultBlend;
		mDefaultState.depth = MTD.defaultDepth;
		mDefaultState.cull = MTD.defaultCull;
		mDefaultState.scissor = MTD.defaultScissor;
		mDefaultState.uniforms[0..mUniforms.length] = mUniforms[];
		mDefaultState.storages[0..mStorages.length] = mStorages[];

		foreach(ref state; mInstanceStates[])
		{
			state = mDefaultState;
		}
	}

	~this()
	{
		Gfx.destroyVertexShader(mDefaultState.vs);
		Gfx.destroyFragmentShader(mDefaultState.fs);

		foreach(i; 0..mUniforms.length)
		{
			Gfx.destroyUniformBuffer(mUniforms[i]);
		}

		foreach(i; 0..mStorages.length)
		{
			Gfx.destroyStorageBuffer(mStorages[i]);
		}

		foreach(i, ref instance; mInstances)
		{
			if(instance is null)
				continue;
			
			instance.destroy();
			instance = null;
		}
	}

	override MaterialInstanceTemplate!MTD createInstance()
	{
		foreach(uint i, ref instance; mInstances[])
		{
			if(instance !is null)
				continue;
			
			instance = new MaterialInstanceTemplate!MTD(i, this);
			return instance;
		}
		return null;
	}
	override MaterialInstanceTemplate!MTD getInstance(uint index) { return null; }
	override void destroyInstance(uint index) { if(mInstances[index]) { mInstances[index].destroy(); mInstances[index] = null; } }
	override const(RenderState)* getDefaultRenderState() { updateBuffers(); return &mDefaultState; }

	const(RenderState*) getRenderState(uint index) const { return &mInstanceStates[index]; }
	void setBlendState(BlendState blend, uint index) { mInstanceStates[index].blend = blend; }
	void setDepthState(DepthState depth, uint index) { mInstanceStates[index].depth = depth; }
	void setCullState(CullState cull, uint index) { mInstanceStates[index].cull = cull; }
	void setScissorState(ScissorState scissor, uint index) { mInstanceStates[index].scissor = scissor; }
	void setTexture(TextureHandle texture, uint texIndex, uint index) { mInstanceStates[index].textures[texIndex] = texture; }
	void setSampler(SamplerHandle sampler, uint texIndex, uint index) { mInstanceStates[index].samplers[texIndex] = sampler; }
	// override const(RenderState)* getRenderState(in PerInstanceState* pis) { updateBuffers(); mCombinedState = mDefaultState; setPerInstanceState(&mCombinedState, pis); return &mCombinedState; }

	void updateBuffers()
	{
		updateUniformBuffers();
		updateStorageBuffers();
	}

	pragma(msg, MaxInstances!(MTD.uniforms));
	pragma(msg, UniformData!(MTD.uniforms));
	pragma(msg, UpdateUniformBuffers!(MTD.uniforms));
	pragma(msg, UpdateStorageBuffers!(MTD.storages));
}

class MaterialInstanceTemplate(alias MTD) : IMaterialInstance if(is(typeof(MTD) == MaterialTemplateDescription!T, T...))
{
	alias description = MTD;

	private
	{
		Texture*[MTD.textureDescriptors.length] mTextures;
		Sampler[MTD.textureDescriptors.length] mSamplers;
		MaterialTemplate!MTD mParent;
		uint mInstance;
	}

	this(uint index, MaterialTemplate!MTD parent)
	{
		mInstance = index;
		mParent = parent;
	}

	override MaterialTemplate!MTD getMaterial() { return mParent; }
	override uint getIndex() { return mInstance; }
	override const(RenderState)* getRenderState() { mParent.updateBuffers(); return mParent.getRenderState(mInstance); }

	mixin(UniformAccessors!(MTD.uniforms));
	mixin(StorageAccessors!(MTD.storages));
	pragma(msg, UniformAccessors!(MTD.uniforms));
	pragma(msg, StorageAccessors!(MTD.storages));

	@property void blendState(BlendState blend) { mParent.setBlendState(blend, mInstance); }
	@property auto blendState() const { return mParent.getRenderState(mInstance).blend; }
	@property void depthState(DepthState depth) { mParent.setDepthState(depth, mInstance); }
	@property auto depthState() const { return mParent.getRenderState(mInstance).depth; }
	@property void cullState(CullState cull) { mParent.setCullState(cull, mInstance); }
	@property auto cullState() const { return mParent.getRenderState(mInstance).cull; }
	@property void scissorState(ScissorState scissor) { mParent.setScissorState(scissor, mInstance); }
	@property auto scissorState() const { return mParent.getRenderState(mInstance).scissor; }

	mixin(TextureAccessors!(MTD.textureDescriptors));
	pragma(msg, TextureAccessors!(MTD.textureDescriptors));
}

struct MaterialTemplateDescription(T...)
{
	string name;
	string vs;
	string fs;
	TextureDescriptor[] textureDescriptors;
	BlendState defaultBlend;
	DepthState defaultDepth;
	CullState defaultCull;
	ScissorState defaultScissor;
	alias uniforms = uniform!T;
	alias storages = storage!T;

	auto toString()
	{
		import std.conv;
		string ret;
		ret ~= "MTD\n(\n";
		ret ~= "\tName: " ~ name ~ "\n";
		ret ~= "\tVS: " ~ vs ~ "\n";
		ret ~= "\tFS: " ~ fs ~ "\n";
		ret ~= "\tTextureDescriptors: " ~ textureDescriptors.to!string ~ "\n";
		ret ~= "\tDefaultBlendState: " ~ defaultBlend.to!string ~ "\n";
		ret ~= "\tDefaultDepthState: " ~ defaultDepth.to!string ~ "\n";
		ret ~= "\tDefaultCullState: " ~ defaultCull.to!string ~ "\n";
		ret ~= "\tDefaultScissorState: " ~ defaultScissor.to!string ~ "\n";
		ret ~= "\tUniforms: " ~ .toString!uniforms ~ "\n";
		ret ~= "\tStorages: " ~ .toString!storages ~ "\n";
		ret ~= ")";
		return ret;
	}
}

// enum TestMTD = MaterialTemplateDescription!(UniformDescriptor!(int[8], "test"), StorageDescriptor!(float, "testFloat"), UniformDescriptor!(float[16], "test2"), StorageDescriptor!(int, "testInt"))(
// 	"Test",
// 	"test.vs",
// 	"test.fs"
// );

// pragma(msg, TestMTD.toString);
// pragma(msg, MaterialTemplate!TestMTD.stringof);
// pragma(msg, MaterialInstanceTemplate!TestMTD.stringof);

struct UniformDescriptor(T, string Name = T.stringof, bool PerInstance = true)
{
	alias type = T;
	alias name = Name;
}

struct StorageDescriptor(T, string Name = T.stringof, bool PerInstance = true)
{
	alias type = T;
	alias name = Name;
}

private
{
	import std.meta : AliasSeq;

	template uniform(T...)
	{
		static if(T.length == 0)
			alias uniform = AliasSeq!();
		else
			static if(is(T[0] == UniformDescriptor!(U, Name), U, string Name))
				alias uniform = AliasSeq!(T[0], uniform!(T[1..$]));
			else
				alias uniform = uniform!(T[1..$]);
			
	}

	template storage(T...)
	{
		static if(T.length == 0)
			alias storage = AliasSeq!();
		else
		{
			static if(is(T[0] == StorageDescriptor!(S, Name), S, string Name))
				alias storage = AliasSeq!(T[0], storage!(T[1..$]));
			else
				alias storage = storage!(T[1..$]);
		}
	}

	template MaxInstances(T...)
	{
		import std.algorithm : min;
		import gfx.common.config;
		static if(T.length == 0)
			enum MaxInstances = MaxUniformBufferSize;
		else
		{
			static assert(Padding!(T[0].type) == 0, "No support for additional padding yet.");
			enum MaxInstances = min(MaxUniformBufferSize / (T[0].type.sizeof + Padding!(T[0].type)), MaxInstances!(T[1..$]));
		}
			
	}

	template Padding(T)
	{
		enum Padding = T.sizeof % 16 == 0 ? 0 : 16 - T.sizeof % 16;
	}

	template PaddedSize(T)
	{
		enum PaddedSize = T.sizeof + Padding!T;
	}

	uint offset(T)(uint idx)
	{
		return cast(uint)((PaddedSize!T) * idx);
	}

	template UniformData(T...)
	{
		enum UniformData = buildData();

		string buildData()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			foreach(i, t; T)
			{
				ret ~= "MTD.uniforms["~i.to!string~"].type[MaxUniformInstances] m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ ";\n";
			}
			
			ret ~= "\n";

			foreach(i, t; T)
			{
				ret ~= "void set" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(MTD.uniforms["~i.to!string~"].type val, uint idx)\n";
				ret ~= "{\n\tm" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[idx] = val;\n";
				ret ~= "\tmUniformsOutOfDate[" ~ i.to!string ~ "] = true;\n";
				// ret ~="\tupdateUniformBuffer(mUniforms[" ~ i.to!string ~ "], cast(ubyte[])m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[offset!(" ~ t.type.stringof ~ ")(idx) .. offset!(" ~ t.type.stringof ~ ")(idx) + PaddedSize!(" ~ t.type.stringof ~ ")], offset!(" ~ t.type.stringof ~ ")(idx));\n";
				ret ~= "}\n"; 
				ret ~= "auto get" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(uint idx) const\n";
				ret ~= "{\n";
				ret ~= "\treturn m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[idx];\n";
				ret ~= "}\n";
			}
			return ret;
		}
	}

	template StorageData(T...)
	{
		enum StorageData = buildData();

		string buildData()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			foreach(i, t; T)
			{
				ret ~= "MTD.storages["~i.to!string~"].type[MaxUniformInstances] m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ ";\n";
			}
			
			ret ~= "\n";

			foreach(i, t; T)
			{
				ret ~= "void set" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(MTD.uniforms["~i.to!string~"].type val, uint idx)\n";
				ret ~= "{\n\tm" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[idx] = val;\n";
				ret ~= "\tmStoragesOutOfDate[" ~ i.to!string ~ "] = true;\n";
				// ret ~="\tupdateUniformBuffer(mUniforms[" ~ i.to!string ~ "], cast(ubyte[])m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[offset!(" ~ t.type.stringof ~ ")(idx) .. offset!(" ~ t.type.stringof ~ ")(idx) + PaddedSize!(" ~ t.type.stringof ~ ")], offset!(" ~ t.type.stringof ~ ")(idx));\n";
				ret ~= "}\n"; 
				ret ~= "auto get" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(uint idx) const\n";
				ret ~= "{\n";
				ret ~= "\treturn m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[idx];\n";
				ret ~= "}\n";
			}
			return ret;
		}
	}

	// template StorageData(T...)
	// {
	// 	enum StorageData = buildData();

	// 	string buildData()
	// 	{
	// 		import std.uni : toUpper;
	// 		import std.conv : to;

	// 		string ret;
	// 		foreach(i, t; T)
	// 		{
	// 			ret ~= t.type.stringof ~ "[MaxUniformInstances] m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ ";\n";
	// 		}
			
	// 		ret ~= "\n";

	// 		foreach(i, t; T)
	// 		{
	// 			ret ~= "void set" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(" ~ t.type.stringof ~ " val, uint idx)\n";
	// 			ret ~= "{\n\tm" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[idx] = val;\n";
	// 			ret ~= "\tmStoragesOutOfDate[" ~ i.to!string ~ "] = true;\n";
	// 			ret ~= "}\n"; 
	// 		}
	// 		return ret;
	// 	}
	// }

	template UpdateUniformBuffers(T...)
	{
		enum UpdateUniformBuffers = buildUpdateFunction();

		string buildUpdateFunction()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			ret ~= "void updateUniformBuffers()\n";
			ret ~= "{\n";
			foreach(i, t; T)
			{
				ret ~= "\tif(mUniformsOutOfDate[" ~ i.to!string ~ "])\n";
				ret ~= "\t\tGfx.updateUniformBuffer(mUniforms[" ~ i.to!string ~ "], cast(ubyte[])m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[]);\n";
			}
			ret ~= "\tmUniformsOutOfDate[] = false;\n";
			ret ~= "}\n";
			return ret;
		}
	}

	template UpdateStorageBuffers(T...)
	{
		enum UpdateStorageBuffers = buildUpdateFunction();

		string buildUpdateFunction()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			ret ~= "void updateStorageBuffers()\n";
			ret ~= "{\n";
			foreach(i, t; T)
			{
				ret ~= "\tif(mStoragesOutOfDate[" ~ i.to!string ~ "])\n";
				ret ~= "\t\tGfx.updateStorageBuffer(mStorages[" ~ i.to!string ~ "], cast(ubyte[])m" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "[]);\n";
			}
			ret ~= "\tmStoragesOutOfDate[] = false;\n";
			ret ~= "}\n";
			return ret;
		}
	}

	template UniformAccessors(T...)
	{
		enum UniformAccessors = buildAccessors();

		string buildAccessors()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			foreach(i, t; T)
			{
				ret ~= "@property auto " ~ t.name ~ "() const  { return mParent.get" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(mInstance); }\n";
				ret ~= "@property void " ~ t.name ~ "(MTD.uniforms["~i.to!string~"].type val" ~ ") { mParent.set" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(val, mInstance); };\n";
			}
			return ret;
		}
	}

	template StorageAccessors(T...)
	{
		enum StorageAccessors = buildAccessors();

		string buildAccessors()
		{
			import std.uni : toUpper;

			string ret;
			foreach(i, t; T)
			{
				ret ~= "@property auto " ~ t.name ~ "() const  { return mParent.get" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(mInstance); }\n";
				ret ~= "@property void " ~ t.name ~ "(MTD.storages["~i.to!string~"].type val" ~ ") { mParent.set" ~ t.name[0..1].toUpper ~ t.name[1..$] ~ "(val, mInstance); };\n";
			}
			return ret;
		}
	}

	template TextureData(alias textures)
	{
		enum TextureData = buildData();

		string buildData()
		{
			import std.uni : toUpper;

			string ret;
			foreach(i, desc; textures)
			{
				ret ~= "Texture* m" ~ desc.name[0..1].toUpper ~ desc.name[1..$] ~ ";\n";
			}
			foreach(i, desc; textures)
			{
				ret ~= "Sampler m" ~ desc.name[0..1].toUpper ~ desc.name[1..$] ~ "Sampler;\n";
			}
			return ret;
		}
	}

	template TextureAccessors(alias textures)
	{
		enum TextureAccessors = buildAccessors();

		string buildAccessors()
		{
			import std.uni : toUpper;
			import std.conv : to;

			string ret;
			foreach(i, desc; textures)
			{
				ret ~= "@property void " ~ desc.name ~ "(Texture* texture)\n";
				ret ~= "{\n";
				ret ~= "\tmTextures[" ~ i.to!string ~ "] = texture;\n";
				ret ~= "\tmParent.setTexture(texture ? texture.handle : TextureHandle.invalid, " ~ i.to!string ~ ", mInstance);\n";
				ret ~= "}\n";
				ret ~= "@property auto " ~ desc.name ~ "() { return mTextures[" ~ i.to!string ~"]; }\n";
			}
			foreach(i, desc; textures)
			{
				ret ~= "@property void " ~ desc.name ~ "Sampler(Sampler sampler)\n";
				ret ~= "{\n";
				ret ~= "\tmSamplers[" ~ i.to!string ~ "] = sampler;\n";
				ret ~= "\tmParent.setSampler(Gfx.getSampler(sampler.min, sampler.mag, sampler.x, sampler.y, sampler.z), " ~ i.to!string ~ ", mInstance);\n";
				ret ~= "}\n";
				ret ~= "@property auto " ~ desc.name ~ "Sampler() { return mSamplers[" ~ i.to!string ~"]; }\n";
			}
			return ret;
		}
	}

	auto toString(T...)()
	{
		string ret = "(\n";
		foreach(t; T)
		{
			ret ~= "\t\t" ~ t.stringof ~ "\n";
		}
		ret ~= "\t)";
		return ret;
	}
}

struct TextureDescriptor
{
	string name;
	Sampler defaultSampler;
}