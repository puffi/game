module game.graphics.light;

import math;
import math.aliases;

struct Light
{
	enum Type
	{
		directional,
		point,
		spot
	}

	Type type;
	vec3 color;
}