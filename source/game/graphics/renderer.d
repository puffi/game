module game.graphics.renderer;

import game.transform;
import game.window;
import game.graphics.mesh;
import game.graphics.material;
import game.graphics.camera;
import game.graphics.light;
import game.graphics.rendergraph;
import game.log;
import math;
import math.aliases;
import gfx.d.api;
import gfx.d.types;

private enum ReservedUniformBufferLocations = 2;
private enum ReservedStorageBufferLocations = 2;

struct Renderer
{
	private
	{
		enum MaxDrawables = 2^^16;

		Drawable[] mDrawables;
		mat4[] mTransforms;
		uint[] mMaterialIndices;

		uint mNumDrawables;

		UniformBufferHandle mViewBuffer;
		StorageBufferHandle mTransformBuffer;
		StorageBufferHandle mMaterialIndicesBuffer;

		Window* mWindow;

		static struct Drawable
		{
			ubyte layer;
			Mesh mesh;
			IMaterialInstance material;
		}

		bool mSorted;
	}

	this(Window* win)
	{
		mWindow = win;

		Gfx.initialize(win.platform);

		mViewBuffer = Gfx.createUniformBuffer(mat4.sizeof * 2);
		mTransformBuffer = Gfx.createStorageBuffer(mat4.sizeof * MaxDrawables);
		mMaterialIndicesBuffer = Gfx.createStorageBuffer(uint.sizeof * MaxDrawables);
	}

	~this()
	{
		Gfx.destroyStorageBuffer(mMaterialIndicesBuffer);
		Gfx.destroyStorageBuffer(mTransformBuffer);
		Gfx.destroyUniformBuffer(mViewBuffer);
	}

	void submit(Renderable* renderable, Transform* transform)
	{
		if(mNumDrawables == MaxDrawables)
		{
			Log.error("[Renderer]: Reached maximum number of renderables.");
			return;
		}

		mDrawables ~= Drawable(renderable.layer, *renderable.mesh, renderable.material);
		mTransforms ~= transform.transform;
		mMaterialIndices ~= renderable.material.getIndex();

		mNumDrawables += 1;

		mSorted = false;
	}

	// void submit(Mesh mesh, IMaterialInstance material, ref Transform transform)
	// {
	// 	if(mNumDrawables == MaxDrawables)
	// 	{
	// 		Log.error("[Renderer]: Reached maximum number of drawables.");
	// 		return;
	// 	}

	// 	mDrawables ~= Drawable(mesh, material);
	// 	mTransforms ~= transform.transform;
	// 	mMaterialIndices ~= material.getIndex();

	// 	mNumDrawables += 1;
	// }

	void submit(Light* light, Transform* transform)
	{

	}

	void render(in Camera* cam, Transform* tr, RenderTargetHandle rt, uint from, uint to)
	{
		if(!mSorted)
		{
			import std.algorithm : sort, SwapStrategy;
			import std.range : zip;

			zip(mDrawables, mTransforms).sort!((a, b) => a[0].layer < b[0].layer)();

			mSorted = true;
		}

		Gfx.setRenderTargets(rt, rt);

		vec2 winSize = vec2(mWindow.width, mWindow.height);
		mat4[2] vp;
		vp[0] = projection(cam, winSize);
		vp[1] = tr.inverseTransform;
		Gfx.updateUniformBuffer(mViewBuffer, cast(ubyte[])vp[]);
		Gfx.updateStorageBuffer(mTransformBuffer, cast(ubyte[])mTransforms[0..mNumDrawables]);
		Gfx.updateStorageBuffer(mMaterialIndicesBuffer, cast(ubyte[])mMaterialIndices[0..mNumDrawables]);

		Gfx.setUniformBuffer(mViewBuffer, 0);
		Gfx.setStorageBuffer(mTransformBuffer, 0);
		Gfx.setStorageBuffer(mMaterialIndicesBuffer, 1);

		Viewport viewport;
		viewport.x = cast(int)(cam.vp.x * winSize.x);
		viewport.y = cast(int)(cam.vp.y * winSize.y);
		viewport.width = cast(int)(cam.vp.w * winSize.x);
		viewport.height = cast(int)(cam.vp.h * winSize.y);
		Gfx.setViewport(viewport);

		import std.algorithm : filter;
		import std.range : assumeSorted, enumerate;
		foreach(uint i, drawable; mDrawables.enumerate!uint.assumeSorted!((a, b) => a[1].layer < b[1].layer).filter!(a => a[1].layer >= from && a[1].layer < to))
		{
			Gfx.setVertexBuffer(drawable.mesh.vb);
			Gfx.setIndexBuffer(drawable.mesh.ib);

			auto rs = drawable.material.getRenderState();
			Gfx.setVertexShader(rs.vs);
			Gfx.setFragmentShader(rs.fs);
			Gfx.setBlendState(rs.blend);
			Gfx.setDepthState(rs.depth);
			Gfx.setCullState(rs.cull);
			
			foreach(uint idx, ub; rs.uniforms[])
			{
				Gfx.setUniformBuffer(ub, ReservedUniformBufferLocations + idx);
			}

			foreach(uint idx, sb; rs.storages[])
			{
				Gfx.setStorageBuffer(sb, ReservedStorageBufferLocations + idx);
			}

			foreach(uint idx, tex; rs.textures[])
			{
				Gfx.setTexture(tex, rs.samplers[idx], idx);
			}

			Gfx.draw(1, i);
		}
	}

	void present()
	{
		Gfx.present(PresentMode.vsync);
	}

	void clear()
	{
		mDrawables.length = 0;
		mDrawables.assumeSafeAppend();
		mTransforms.length = 0;
		mTransforms.assumeSafeAppend();
		mNumDrawables = 0;

		Gfx.clearRenderTarget(DefaultRenderTarget);

		mSorted = false;
	}
}

struct Renderable
{
	Mesh* mesh;
	IMaterialInstance material;
	ubyte layer;
}

private
{
	auto projection(in Camera* cam, vec2 size)
	{
		auto ar = (size.x*cam.vp.w)/(size.y*cam.vp.h);
		final switch(cam.type)
		{
			case Camera.ProjectionType.perspective:
				return perspective!(Unit.degree)(cam.yfov, ar, cam.near, cam.far);
			case Camera.ProjectionType.orthographic:
				return orthographic(cam.verticalSize * ar / 2, cam.verticalSize / 2, cam.near, cam.far);
			case Camera.ProjectionType.orthographicPP:
				return orthographic(size.x * cam.vp.w / 2, size.y * cam.vp.h / 2, cam.near, cam.far);
		}
	}

	struct LightData
	{
		vec3 ambient;
		uint numLights;
	}

	struct PerLightData
	{
		vec4 position;
		float strength;
	}
}