module game.ecs;

alias EntityIdType = uint;
enum EntityIdReservedBits = 1;
enum EntityIdReservedBitsMask = ((1 << EntityIdReservedBits) - 1) << EntityIdReservedBitsShift;
enum EntityIdReservedBitsShift = (EntityIdType.sizeof * 8) - EntityIdReservedBits;
enum EntityIdRevBits = (EntityIdType.sizeof * 8) / 4 - EntityIdReservedBits;
enum EntityIdRevMask = ((1 << EntityIdRevBits) - 1) << EntityIdRevShift;
enum EntityIdRevShift = EntityIdReservedBitsShift - EntityIdRevBits;
enum EntityIdBits = EntityIdType.sizeof * 8 - (EntityIdReservedBits + EntityIdRevBits);
enum EntityIdMask = ((1 << EntityIdBits) - 1) << EntityIdShift;
enum EntityIdShift = EntityIdRevShift - EntityIdBits;

enum EntityInUseFlag = 1 << (EntityIdType.sizeof * 8 - 1);

static assert(EntityIdReservedBitsMask == 0x80000000);
static assert(EntityIdRevBits == 7);
static assert(EntityIdRevMask == 0x7f000000);
static assert(EntityIdMask == 0x00ffffff);
static assert(EntityIdBits + EntityIdRevBits == 31);

enum MaxEntities = (1 << EntityIdBits) - 1;
enum InvalidEntityId = MaxEntities;

pragma(msg, "MaxEntities: ", MaxEntities);
pragma(msg, "InvalidEntityId: ", InvalidEntityId);
static assert((MaxEntities & EntityIdRevMask) == 0);

import std.meta : allSatisfy;
import game.log;

struct Entity
{
	private
	{
		EntityIdType mId;
	}

	@property auto rev() const { return (mId & EntityIdRevMask) >> EntityIdRevShift; }
	@property auto id() const { return (mId & EntityIdMask) >> EntityIdShift; }

	@property auto data() const { return mId; }

	@property auto isValid() const { return mId != InvalidEntityId; }
	static @property auto invalid() { return Entity(EntityIdType.max); }

	auto opCmp(Entity rhs) const
	{
		return rhs.id - id;
	}

	string toString() const
	{
		import std.conv : to;
		return "Entity(" ~ id.to!string ~ ", rev: " ~ rev.to!string ~ ")";
	}
}

struct ECS
{
	private
	{
		Entity[MaxEntities] mEntities;

		bool isInUse(EntityIdType id)
		{
			auto reservedBits = mEntities[id].data & EntityIdReservedBitsMask;
			if(mEntities[id].data & EntityInUseFlag)
				return true;
			return false;
		}
	}

	void initialize()
	{
		foreach(EntityIdType i, ref e; mEntities[])
		{
			e = Entity(i << EntityIdShift);
		}
	}

	Entity create()
	{
		foreach(ref e; mEntities[])
		{
			if(isInUse(e.id))
				continue;
			
			e = Entity(EntityInUseFlag | e.rev << EntityIdRevShift | e.id << EntityIdShift);
			return Entity(e.rev << EntityIdRevShift | e.id << EntityIdShift);
		}

		return Entity.invalid;
	}

	void destroy(Entity e)
	{
		if(!e.isValid)
			return;
		
		mEntities[e.id] = Entity(((e.rev + 1) << EntityIdRevShift) & EntityIdRevMask | e.id << EntityIdShift);
	}

	private auto getComponentManager(T)() if(isComponent!T)
	{
		static ComponentManager!T* compMgr;
		if(!compMgr)
			compMgr = new ComponentManager!T();
		return compMgr;
	}

	auto add(T)(Entity e) if(isComponent!T)
	{
		return getComponentManager!T.add(e);
	}

	auto get(T)(Entity e) if(isComponent!T)
	{
		return getComponentManager!T.get(e);
	}

	void remove(T)(Entity e) if(isComponent!T)
	{
		getComponentManager!T.remove(e);
	}

	
	auto join(T...)() if(allSatisfy!(isComponent, T))
	{
		import std.meta : staticMap;
		alias Components = staticMap!(Pointer, T);
		import std.typecons : Tuple, tuple;
		static struct JoinRange(T...)
		{
			private
			{
				
				Tuple!(Entity[], staticMap!(PointerArray, T)) mPayload;
			}

			@property auto payload() { return mPayload; }

			@property auto front()
			{
				alias RetType = Tuple!(Entity, Components);
				RetType ret;
				ret[0] = mPayload[0][0];
				foreach(i, _; Components)
				{
					ret[i+1] = mPayload[i+1][0];
				}
				return ret;
			}

			@property auto empty() { return mPayload[0].length == 0; }

			void popFront()
			{
				foreach(i, _; mPayload)
				{
					import std.range : popFront;
					mPayload[i].popFront();
				}
			}
		}

		import std.meta : ApplyLeft;
		Tuple!(staticMap!(EntityRange, T)) nonIntersectedEntities;
		foreach(i, ref ents; nonIntersectedEntities)
		{
			ents = getComponentManager!(T[i]).entities;
		}
		
		Tuple!(staticMap!(PointerArray, T)) nonIntersectedComponents;
		foreach(i, ref comp; nonIntersectedComponents)
		{
			comp = getComponentManager!(T[i]).components;
		}

		import std.algorithm : setIntersection;
		import std.range : array;
		static if(T.length > 1)
			Entity[] entities = setIntersection(nonIntersectedEntities.expand).array;
		else
			Entity[] entities = nonIntersectedEntities[0];
		
		import std.algorithm : filter;
		import std.range : array;
		entities = entities.filter!(a => isValid(a)).array;

		Tuple!(staticMap!(PointerArray, T)) components;
		foreach(i, ref comp; components)
		{
			import std.algorithm : filter, canFind, map;
			import std.range : zip, array;
			comp = zip(nonIntersectedEntities[i], nonIntersectedComponents[i]).filter!(a => canFind(entities, a[0])).map!(a => a[1]).array;
		}
		return JoinRange!T(tuple(entities, components.expand));
	}

	bool isValid(Entity e)
	{
		if(!e.isValid)
			return false;
		
		if(!isInUse(e.id))
			return false;
		
		if(mEntities[e.id].rev != e.rev)
			return false;
		
		return true;
	}
}



template isComponent(T)
{
	static if(is(T == struct))
		enum isComponent = true;
	else
		static assert(false, "Only structs supported as components for now.");
}

struct ComponentManager(T) if(isComponent!T)
{
	private
	{
		Entity[] mEntities;
		T*[] mComponents;
		PoolAllocator!T mAllocator;

		bool mIsSorted;
	}

	auto add(Entity e)
	{
		import std.algorithm : countUntil;
		auto idx = mEntities.countUntil(e);
		if(idx < 0)
		{
			mEntities ~= e;
			auto ptr = mAllocator.allocate();
			mComponents ~= ptr;
			mIsSorted = false;
			return ptr;
		}
		else
		{
			// override Entity.rev
			mEntities[idx] = e;
			return mComponents[idx];
		}
	}

	auto get(Entity e)
	{
		sort();
		import std.algorithm : countUntil;
		import std.range : assumeSorted;
		auto idx = mEntities.assumeSorted.countUntil(e);
		if(idx < 0)
			return null;
		if(mEntities[idx] != e)
			return null;
		return mComponents[idx];
	}

	void remove(Entity e)
	{
		import std.algorithm : countUntil;

		auto idx = mEntities.countUntil(e);
		if(idx < 0)
			return;
		

		import std.algorithm : remove, SwapStrategy;
		mEntities = mEntities.remove!(SwapStrategy.unstable)(idx);
		auto comp = mComponents[idx];
		mComponents = mComponents.remove!(SwapStrategy.unstable)(idx);
		mAllocator.free(comp);

		mEntities.assumeSafeAppend();
		mComponents.assumeSafeAppend();

		mIsSorted = false;
	}

	private void sort()
	{
		if(mIsSorted)
			return;

		import std.algorithm : sort;
		import std.range : zip;
		mEntities.zip(mComponents).sort!((a, b) => a[0] < b[0])();

		mIsSorted = true;
	}

	@property auto entities() { sort(); return mEntities; }
	@property auto components() { sort(); return mComponents; }
}

struct PoolAllocator(T)
{
	enum PoolSize = 256;
	struct Pool
	{
		T[PoolSize] mPayload;
		FreeSpace[] mFreeSpace = [
			FreeSpace(0, PoolSize)
		];

		struct FreeSpace
		{
			uint index;
			uint count;
		}

		@property auto first() { return &mPayload[0]; }
		@property auto last() { return &mPayload[$-1]; }

		auto allocate()
		{
			if(mFreeSpace.length == 0)
				return null;
			
			auto space = mFreeSpace[0];
			import std.algorithm : remove, SwapStrategy;
			mFreeSpace = mFreeSpace.remove!(SwapStrategy.stable)(0);
			mFreeSpace.assumeSafeAppend();
			return &mPayload[space.index];
		}

		void free(T* ptr)
		{
			assert(ptr >= first);
			assert(ptr <= last);

			uint index = cast(uint)(ptr - first);
			FreeSpace space;
			space.index = index;
			space.count = 1;
			mFreeSpace ~= space;
			mPayload[index] = T.init;

			optimize();
		}

		void optimize()
		{
			import std.algorithm : sort;
			mFreeSpace.sort!((a, b) => a.index < b.index);
			import std.range : retro, enumerate;
			import std.algorithm : remove, SwapStrategy;
			foreach(i, ref space; mFreeSpace.enumerate.retro)
			{
				if(i > 0)
				{
					auto previous = mFreeSpace[i-1];
					if(previous.index + previous.count == space.index)
					{
						previous.count += space.count;
						mFreeSpace = mFreeSpace.remove!(SwapStrategy.stable)(i);
					}
				}
			}

			mFreeSpace.assumeSafeAppend();
		}
	}

	Pool*[] pools;
	
	auto allocate()
	{
		foreach(pool; pools)
		{
			auto ptr = pool.allocate();
			if(!ptr)
				continue;
			
			return ptr;
		}

		pools ~= new Pool();
		return allocate();
	}

	void free(T* ptr)
	{
		foreach(pool; pools)
		{
			if(ptr >= pool.first && ptr <= pool.last)
			{
				pool.free(ptr);
				return;
			}
		}
		assert(false);
	}
}

template Pointer(T)
{
	alias Pointer = T*;
}

template PointerArray(T)
{
	alias PointerArray = T*[];
}

template ComponentManagerPointer(T)
{
	alias ComponentManagerPointer = ComponentManager!T*;
}

template EntityRange(T)
{
	alias EntityRange = Entity[];
}