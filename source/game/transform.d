module game.transform;

import math;
import math.aliases;

struct Transform
{
	private
	{
		mat4 mTransform = mat4.identity;
		quat mWorldOrientation = quat.identity;
		quat mLocalOrientation = quat.identity;
		vec3 mWorldPosition = vec3(0);
		vec3 mLocalPosition = vec3(0);
		vec3 mWorldScale = vec3(1);
		vec3 mLocalScale = vec3(1);
		
		Transform*[] mChildren;
		Transform* mParent;
		
		bool mNeedsRebuild;
		
		
		
		void update()
		{
			if(mParent)
			{
				mWorldOrientation = (mParent.orientation * mLocalOrientation).normalize;
				mWorldScale = mParent.scale * mLocalScale;
				mWorldPosition = mParent.orientation * (mParent.scale * mLocalPosition);
				mWorldPosition = mWorldPosition + mParent.position;
			}
			else
			{
				mWorldOrientation = mLocalOrientation;
				mWorldPosition = mLocalPosition;
				mWorldScale = mLocalScale;
			}
			
			mNeedsRebuild = true;
			
			updateChildren();
		}
		
		void updateChildren()
		{
			foreach(c; mChildren)
				c.update();
		}
		
		void addChild(Transform* child)
		{
			mChildren ~= child;
		}
		
		void removeChild(Transform* child)
		{
			import std.algorithm : SwapStrategy, remove;
			foreach(i, c; mChildren)
			{
				if(c == child)
				{
					mChildren = remove!(SwapStrategy.unstable)(mChildren, i);
					mChildren.assumeSafeAppend();
					return;
				}
			}
		}
		
		void setParent(Transform* parent)
		{
			if(parent != mParent)
			{
				if(mParent)
				{
					mParent.removeChild(&this);
				}
				mParent = parent;
				if(mParent)
				{
					mParent.addChild(&this);
				}
				update();
			}
		}
		
		void rebuild()
		{
			mTransform = math.translate(mWorldPosition) * math.scale(mWorldScale) * math.rotate(mWorldOrientation);
			
			mNeedsRebuild = false;
		}
	}
	
	@property Transform* parent() { return mParent; }
	@property void parent(Transform* val) { setParent(val); }
	@property Transform*[] children() { return mChildren; }
	
	@property mat4 transform() { if(mNeedsRebuild) rebuild(); return mTransform; }
	@property mat4 inverseTransform() { return math.translate(mWorldPosition.negate) * math.scale(1 / mWorldScale) * math.rotate(mWorldOrientation.invert); }
	@property quat orientation() { return mWorldOrientation; }
	@property void orientation(quat val) { if(mParent) localOrientation = mParent.transformOrientationWtoL(val); else localOrientation = val; }
	@property quat localOrientation() { return mLocalOrientation; }
	@property void localOrientation(quat val) { mLocalOrientation = normalize(val); update(); }
	@property vec3 position() { return mWorldPosition; }
	@property void position(vec3 val) { if(mParent) localPosition = mParent.transformPointWtoL(val); else localPosition = val; }
	@property vec3 localPosition() { return mLocalPosition; }
	@property void localPosition(vec3 val) { mLocalPosition = val; update(); }
	@property vec3 scale() { return mWorldScale; }
	@property vec3 localScale() { return mLocalScale; }
	@property void localScale(vec3 val) { mLocalScale = val; update(); }
	
	@property vec3 forward() { return transformDirectionLtoW(vec3(0, 0, -1)); }
	
	vec3 transformPointWtoL(vec3 point)
	{
		return mWorldOrientation.conjugate * (point - mLocalPosition) / mLocalScale;
	}
	
	vec3 transformPointLtoW(vec3 point)
	{
		return mWorldOrientation * (point * mWorldScale) + mWorldPosition;
	}
	
	vec3 transformDirectionWtoL(vec3 dir)
	{
		return mWorldOrientation.conjugate * dir;
	}
	
	vec3 transformDirectionLtoW(vec3 dir)
	{
		return mWorldOrientation * dir;
	}
	
	quat transformOrientationWtoL(quat or)
	{
		return mWorldOrientation.conjugate * or;
	}
	
	quat transformOrientationLtoW(quat or)
	{
		return mWorldOrientation * or;
	}
	
	void translate(vec3 translation, TransformSpace space = TransformSpace.parent)
	{
		final switch(space)
		{
			case TransformSpace.local:
				mLocalPosition = mLocalPosition + (mLocalOrientation * translation);
				break;
			case TransformSpace.parent:
				mLocalPosition = mLocalPosition + translation;
				break;
			case TransformSpace.world:
				if(mParent)
				{
					mLocalPosition = mLocalPosition + ((mParent.orientation.conjugate * translation) / mParent.scale);
				}
				else
				{
					mLocalPosition = mLocalPosition + translation;
				}
				break;
		}
		
		update();
	}
	
	void rotate(quat rot, TransformSpace space = TransformSpace.local)
	{
		rot = normalize(rot);
		
		final switch(space)
		{
			case TransformSpace.local:
				mLocalOrientation = normalize(mLocalOrientation * rot);
				break;
			case TransformSpace.parent:
				mLocalOrientation = normalize(rot * mLocalOrientation);
				break;
			case TransformSpace.world:
				mLocalOrientation = normalize(mLocalOrientation * mWorldOrientation.conjugate * rot * mWorldOrientation);
				break;
		}
		
		update();
	}
	
	void rotateAround(quat rot, vec3 point)
	{
		rot = normalize(rot);
		
		vec3 temp = position - point;
		
		translate(point - position, TransformSpace.world);
		rotate(rot);
		translate(rot * temp, TransformSpace.world);
	}
	
	string toString()
	{
		import std.format;
		return format("[Transform]:\n\t[Mat]: %s\n\t[Pos]: %s\n\t[Orientation]; %s\n\t[Scale]: %s\n\t[LocalPos]: %s\n\t[LocalOrientation]: %s\n\t[LocalScale]: %s\n\t[Children]: %s", mTransform, mWorldPosition, mWorldOrientation, mWorldScale, mLocalPosition, mLocalOrientation, mLocalScale, mChildren.length);
	}
}

enum TransformSpace
{
	local,
	parent,
	world
}