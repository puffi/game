module game.time;

import game.log;
import core.time;


private alias ClockImpl = MonoTimeImpl!(ClockType.raw);

struct Clock
{
	static nothrow:
	@property auto currentTime()
	{
		return Time(ClockImpl.currTime.ticks);
	}

	@property auto frequency()
	{
		return ClockImpl.ticksPerSecond;
	}
}

struct Timer
{
	private
	{
		Time mCurrentTime;
	}

	auto get() const nothrow
	{
		return Clock.currentTime - mCurrentTime;
	}

	auto restart() nothrow
	{
		auto now = Clock.currentTime;
		auto dur = now - mCurrentTime;
		mCurrentTime = now;
		return dur;
	}

	static auto opCall() nothrow
	{
		Timer timer;
		timer.restart();
		return timer;
	} 
}

struct Time
{
	private
	{
		ulong mTimestamp;
	}

	@property auto timestamp() const nothrow { return mTimestamp; }

	auto opBinary(string op)(Time rhs) if(op == "+" || op == "-")
	{
		mixin("return Duration(mTimestamp "~op~"rhs.mTimestamp);");
	}

	long opCmp(Time rhs)
	{
		return rhs.mTimestamp - mTimestamp;
	}
}

struct Duration
{
	private
	{
		ulong mTicks;
	}

	@property auto ticks() const nothrow { return mTicks; }

	T as(T, string unit = "seconds")() const
	{		
		import std.traits;
		static if(isFloatingPoint!T || isIntegral!T)
			T ret = cast(T)(mTicks) / cast(T)Clock.frequency;
		else static assert(false, "Type " ~ T.stringof ~ " not supported.");
		
		static if(unit == "s" || unit == "seconds")
			return ret;
		else static if(unit == "ms" || unit == "milliseconds")
			return ret * 1000;
		else static if(unit == "µs" || unit == "microseconds")
			return ret * 1000 * 1000;
		else static assert(false, "Unit " ~ unit ~ " not supported.");
	}

	auto opBinary(string op)(Duration rhs) if(op == "+" || op == "-")
	{
		mixin("return Duration(mTicks "~op~" rhs.mTicks);");
	}

	auto opBinary(string op)(Time rhs) if(op == "+" || op == "-")
	{
		mixin("return Time(mTicks "~op~" rhs.mTimestamp);");
	}

	auto opBinaryRight(string op)(Time lhs) if(op == "+" || op == "-")
	{
		mixin("return Time(lhs.mTimestamp "~op~" mTicks);");
	}

	long opCmp(in Duration rhs) const nothrow
	{
		return rhs.mTicks - mTicks;
	}
}

import std.traits : isFloatingPoint, isIntegral;
Duration duration(T, string unit = "seconds")(T value) if(isFloatingPoint!T || isIntegral!T)
{
		static if(unit == "s" || unit == "seconds")
			long ticks = cast(long)(value * Clock.frequency);
		else static if(unit == "ms" || unit == "milliseconds")
			long ticks = cast(long)((value * Clock.frequency) / 1_000);
		else static if(unit == "µs" || unit == "microseconds")
			long ticks = cast(long)((value * Clock.frequency) / 1_000_000);
		else static assert(false, "Unit " ~ unit ~ " not supported.");
		
		return Duration(ticks);
}