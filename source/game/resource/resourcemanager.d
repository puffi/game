module game.resource.resourcemanager;

struct ResourceManager
{

}

interface IResourceManager
{
	
}

struct Resource(T)
{
	private
	{
		Status mStatus;
		T mResource;
	}

	enum Status
	{
		unloaded,
		loaded,
		ready
	}

	@property auto get()
	{
		return &mResource;
	}

	@property auto status() { return mStatus; }

	alias get this;
}