module game.input;

import game.window : KeyEvent, MouseButtonEvent,
	MouseWheelEvent, MouseMoveEvent,
	Scancode, Keycode, Modifier, toKeycode, toScancode;

struct InputManager
{
	private
	{
		Action[string] mActions;
		bool[Scancode.NUM_SCANCODES] mKeys;
	}

	auto isKeyDown(Scancode s)
	{
		return mKeys[s];	
	}

	bool registerAction(string name, Action action)
	{
		if(auto a = name in mActions)
			return false;
		
		mActions[name] = action;
		return true;
	}

	bool deregisterAction(string name)
	{
		return mActions.remove(name);
	}

	auto getAction(string name)
	{
		return name in mActions;
	}

	void input(KeyEvent evt)
	{
		if(evt.type == KeyEvent.Type.press)
		{
			mKeys[evt.scancode] = true;
		}
		else if(evt.type == KeyEvent.Type.release)
		{
			mKeys[evt.scancode] = false;
		}
	}
}

struct Action
{
	enum Type
	{
		onPress,
		onRelease
	}
	Type type;
	Scancode key;
	Modifier mod;

	void delegate()[] callbacks;
}

struct Axis
{

}

private
{
	struct InputSet
	{
		bool[Scancode] mKeys;

	}
}