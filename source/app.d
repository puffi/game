import std.stdio;

import game.window;
import game.log;
import game.transform;
import game.ecs;
import game.graphics.renderer;
import game.graphics.mesh;
import game.graphics.material;
import game.graphics.camera;
import game.graphics.texture;
import game.graphics.rendergraph;

import gfx.d.api;
import gfx.d.types;

import math;
import math.aliases;

import imageformats;

void main()
{
	auto win = new Window("rpg3d", 1024, 768);
	auto renderer = new Renderer(win);
	auto ecs = new ECS();
	ecs.initialize();
	
	auto camEntity = ecs.create();
	auto cubeEntity = ecs.create();
	auto cubeEntity2 = ecs.create();

	Camera* cam = ecs.add!Camera(camEntity);
	cam.type = Camera.ProjectionType.perspective;
	cam.yfov = 45;

	Mesh mesh;
	createMesh(mesh, CubeVertices[], CubeIndices[]);

	auto cubeMat = new MaterialTemplate!CubeMaterial;
	auto cubeMatInstance = cubeMat.createInstance();
	cubeMatInstance.color = Color(1, 1, 1, 1);
	auto cubeMatInstance2 = cubeMat.createInstance();
	cubeMatInstance2.color = Color(1, 1, 1, 1);

	Transform* camTr = ecs.add!Transform(camEntity);
	Transform* cubeTr = ecs.add!Transform(cubeEntity);
	Transform* cubeTr2 = ecs.add!Transform(cubeEntity2);

	auto renderable = ecs.add!Renderable(cubeEntity);
	auto renderable2 = ecs.add!Renderable(cubeEntity2);
	renderable.mesh = &mesh;
	renderable.material = cubeMatInstance;
	renderable2.mesh = &mesh;
	renderable2.material = cubeMatInstance2;

	camTr.position = vec3(0, 2, 10);
	cubeTr.position = vec3(-2, 0, 0);
	cubeTr2.position = vec3(2, 0, 0);

	IFImage image = read_image("resources/textures/untitled.png");
	auto texHandle = Gfx.createTexture2D(image.w, image.h, Format.rgba8, 1);
	Gfx.updateTexture(texHandle, 0, 0, 0, 0, image.w, image.h, 0, image.pixels);
	Texture tex;
	tex.width = image.w;
	tex.height = image.h;
	tex.handle = texHandle;
	tex.type = Texture.Type._2d;
	cubeMatInstance.albedo = &tex;
	cubeMatInstance.albedoSampler = Sampler(Filter.linear, Filter.linear, Wrap.repeat, Wrap.repeat, Wrap.repeat);
	cubeMatInstance2.albedo = &tex;
	cubeMatInstance2.albedoSampler = Sampler(Filter.nearest, Filter.nearest, Wrap.repeat, Wrap.repeat, Wrap.repeat);

	auto colSpecTex = Gfx.createTexture2D(win.width, win.height, Format.rgba8, 1);
	auto posTex = Gfx.createTexture2D(win.width, win.height, Format.rgb16f, 1);
	auto normalTex = Gfx.createTexture2D(win.width, win.height, Format.rgb16f, 1);
	auto depthTex = Gfx.createTexture2D(win.width, win.height, Format.depth32, 1);

	TextureHandle[Attachment.max+1] texAttachments;
	texAttachments[Attachment.color0] = posTex;
	texAttachments[Attachment.color1] = normalTex;
	texAttachments[Attachment.color2] = colSpecTex;
	texAttachments[Attachment.depth] = depthTex;
	RTClearValue[Attachment.max+1] clearValues;
	clearValues[Attachment.color2].type = RTClearValue.Type.vec4;
	clearValues[Attachment.color2].vec4 = vec4(0, 0, 0, 0).ptr[0..4];
	clearValues[Attachment.depth].type = RTClearValue.Type.f32;
	clearValues[Attachment.depth].f32 = 1;

	auto rt = Gfx.createRenderTarget(texAttachments, clearValues);
	Gfx.clearRenderTarget(rt);

	auto blitMat = new MaterialTemplate!BlitMaterial;
	auto blitMatInstance = blitMat.createInstance();
	Texture blitPosTex;
	blitPosTex.width = win.width;
	blitPosTex.height = win.height;
	blitPosTex.handle = posTex;
	blitPosTex.type = Texture.Type._2d;
	blitMatInstance.readPosTexture = &blitPosTex;
	blitMatInstance.readPosTextureSampler = Sampler.init;
	Texture blitNormalTex;
	blitNormalTex.width = win.width;
	blitNormalTex.height = win.height;
	blitNormalTex.handle = posTex;
	blitNormalTex.type = Texture.Type._2d;
	blitMatInstance.readNormalTexture = &blitNormalTex;
	blitMatInstance.readNormalTextureSampler = Sampler.init;
	Texture blitColorSpecTex;
	blitColorSpecTex.width = win.width;
	blitColorSpecTex.height = win.height;
	blitColorSpecTex.handle = colSpecTex;
	blitColorSpecTex.type = Texture.Type._2d;
	blitMatInstance.readColSpecTexture = &blitColorSpecTex;
	blitMatInstance.readColSpecTextureSampler = Sampler.init;

	Mesh quadMesh;
	createMesh(quadMesh, QuadVertices[], QuadIndices[]);

	Renderable blitRenderable;
	blitRenderable.layer = 255;
	blitRenderable.mesh = &quadMesh;
	blitRenderable.material = blitMatInstance;

	Transform blitTransform;

	while(win.isOpen)
	{
		renderer.clear();
		Gfx.clearRenderTarget(rt);

		// renderer.submit(mesh, cubeMatInstance, cubeTr);
		// renderer.submit(mesh, cubeMatInstance2, cubeTr2);
		submitRenderables(renderer, ecs);
		renderer.submit(&blitRenderable, &blitTransform);

		renderer.render(cam, camTr, rt, 0, 255);
		renderer.render(cam, camTr, DefaultRenderTarget, 255, 256);

		renderer.present();

		Event evt;
		while(win.pollEvent(evt))
		{
			final switch(evt.type)
			{
				case Event.Type.quit: win.close(); break;
				case Event.Type.window:
					final switch(evt.window.type)
					{
						case WindowEvent.Type.close: win.close(); break;
						case WindowEvent.Type.invalid: break;
					}
					break;
				case Event.Type.key: break;
				case Event.Type.mouseButton: break;
				case Event.Type.mouseWheel: break;
				case Event.Type.mouseMove: break;
				case Event.Type.invalid: break;
			}
		}
	}

}

void submitRenderables(Renderer* renderer, ECS* ecs)
{
	foreach(e, r, tr; ecs.join!(Renderable, Transform))
	{
		// renderer.submit(*r.mesh, r.material, *tr);
		renderer.submit(r, tr);
	}
}


void createMesh(Vertex, Index)(ref Mesh mesh, in Vertex[] vertices, in Index[] indices)
{
	static immutable Attribs = Attributes!Vertex;
	static immutable IT = toIndexType!Index;

	mesh.vb = Gfx.createVertexBuffer(cast(ubyte[])vertices[], Attribs[]);
	mesh.ib = Gfx.createIndexBuffer(cast(ubyte[])indices[], IT);
}

struct Color
{
	float r;
	float g;
	float b;
	float a;
}

enum CubeMaterial = MaterialTemplateDescription!(
	UniformDescriptor!(Color, "color")
)(
	"CubeMaterial",
	VS,
	FS,
	[
		TextureDescriptor("albedo")
	],
	BlendState.init,
	DepthState(true, DepthState.Func.lequal)
);

enum BlitMaterial = MaterialTemplateDescription!()(
	"BlitMaterial",
	BlitVS,
	BlitFS,
	[
		TextureDescriptor("readPosTexture"),
		TextureDescriptor("readNormalTexture"),
		TextureDescriptor("readColSpecTexture")
	]
);

private
{
	template Attributes(Vertex)
	{
		private static immutable StaticAttributes = buildAttributes();
		enum Attributes = StaticAttributes[0..numAttributes];


		auto buildAttributes()
		{
			import gfx.common.config;
			Attribute[MaxAttributes] attributes;

			import std.traits : FieldNameTuple, hasUDA, getUDAs;
			foreach(i, mem; FieldNameTuple!Vertex)
			{
				static assert(hasUDA!(mixin("Vertex."~mem), Attribute));
				attributes[i] = getUDAs!(mixin("Vertex."~mem), Attribute)[0];
			}
			return attributes;
		}

		auto numAttributes()
		{
			import std.traits : FieldNameTuple, hasUDA;
			ulong count;
			foreach(i, mem; FieldNameTuple!Vertex)
			{
				if(hasUDA!(mixin("Vertex."~mem), Attribute))
					count += 1;
			}
			return count;
		}
	}

	template toIndexType(T)
	{
		static if(is(T == ushort))
			enum toIndexType = IndexType.u16;
		else static if(is(T == uint))
			enum toIndexType = IndexType.u32;
		else
			static assert(false, "Invalid index type");
	}
}

struct Vertex
{
	@Attribute(0, Attribute.Type.f32, 3, false)
	vec3 position;

	@Attribute(1, Attribute.Type.f32, 3, false)
	vec3 normal;

	@Attribute(2, Attribute.Type.f32, 2, false)
	vec2 uv;

	@Attribute(3, Attribute.Type.u8, 4, true)
	ubvec4 color;
}

enum VS = "
#version 450 core

#extension GL_ARB_shader_draw_parameters: require

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec4 color;

layout(location = 0) out vec2 o_uv;
layout(location = 1) out vec4 o_color;

layout(std140, binding = 0) uniform VP
{
	mat4 proj;
	mat4 view;
} vp;

layout(std430, binding = 0) buffer Transform
{
	mat4[] transforms;
};

layout(std430, binding = 1) buffer MaterialIndices
{
	uvec4[] matIndices;
};

layout(std140, binding = 2) uniform Color
{
	vec4[4096] colorData;
};

uint getIndex()
{
	return uint(matIndices[gl_BaseInstanceARB / 4][gl_BaseInstanceARB % 4]);
}

void main()
{
	uint index = getIndex();
	gl_Position = vp.proj * vp.view * transforms[gl_BaseInstanceARB] * vec4(position, 1);
	o_uv = uv;
	o_color = color * colorData[index];
}

";

enum FS = "
#version 450 core

layout(location = 0) in vec2 uv;
layout(location = 1) in vec4 color;

layout(location = 0) out vec3 o_pos;
layout(location = 1) out vec3 o_normal;
layout(location = 2) out vec4 o_color;

layout(binding = 0) uniform sampler2D tex0;

void main()
{
	o_color = texture(tex0, uv) * color;
}
";

static immutable Vertex[24] CubeVertices = [
	// front
	Vertex(vec3(-1, -1,  1), vec3( 0,  0,  1), vec2(0, 0), ubvec4(255)),
	Vertex(vec3( 1, -1,  1), vec3( 0,  0,  1), vec2(1, 0), ubvec4(255)),
	Vertex(vec3( 1,  1,  1), vec3( 0,  0,  1), vec2(1, 1), ubvec4(255)),
	Vertex(vec3(-1,  1,  1), vec3( 0,  0,  1), vec2(0, 1), ubvec4(255)),
	// right
	Vertex(vec3( 1, -1,  1), vec3( 1,  0,  0), vec2(0, 0), ubvec4(255)),
	Vertex(vec3( 1, -1, -1), vec3( 1,  0,  0), vec2(1, 0), ubvec4(255)),
	Vertex(vec3( 1,  1, -1), vec3( 1,  0,  0), vec2(1, 1), ubvec4(255)),
	Vertex(vec3( 1,  1,  1), vec3( 1,  0,  0), vec2(0, 1), ubvec4(255)),
	// back
	Vertex(vec3( 1, -1, -1), vec3( 0,  0, -1), vec2(0, 0), ubvec4(255)),
	Vertex(vec3(-1, -1, -1), vec3( 0,  0, -1), vec2(1, 0), ubvec4(255)),
	Vertex(vec3(-1,  1, -1), vec3( 0,  0, -1), vec2(1, 1), ubvec4(255)),
	Vertex(vec3( 1,  1, -1), vec3( 0,  0, -1), vec2(0, 1), ubvec4(255)),
	// left
	Vertex(vec3(-1, -1, -1), vec3(-1,  0,  0), vec2(0, 0), ubvec4(255)),
	Vertex(vec3(-1, -1,  1), vec3(-1,  0,  0), vec2(1, 0), ubvec4(255)),
	Vertex(vec3(-1,  1,  1), vec3(-1,  0,  0), vec2(1, 1), ubvec4(255)),
	Vertex(vec3(-1,  1, -1), vec3(-1,  0,  0), vec2(0, 1), ubvec4(255)),
	// bottom
	Vertex(vec3(-1, -1, -1), vec3( 0, -1,  0), vec2(0, 0), ubvec4(255)),
	Vertex(vec3( 1, -1, -1), vec3( 0, -1,  0), vec2(1, 0), ubvec4(255)),
	Vertex(vec3( 1, -1,  1), vec3( 0, -1,  0), vec2(1, 1), ubvec4(255)),
	Vertex(vec3(-1, -1,  1), vec3( 0, -1,  0), vec2(0, 1), ubvec4(255)),
	// top
	Vertex(vec3(-1,  1,  1), vec3( 0,  1,  0), vec2(0, 0), ubvec4(255)),
	Vertex(vec3( 1,  1,  1), vec3( 0,  1,  0), vec2(1, 0), ubvec4(255)),
	Vertex(vec3( 1,  1, -1), vec3( 0,  1,  0), vec2(1, 1), ubvec4(255)),
	Vertex(vec3(-1,  1, -1), vec3( 0,  1,  0), vec2(0, 1), ubvec4(255))
];

static immutable uint[36] CubeIndices = [
	// front
	0, 1, 2,
	2, 3, 0,
	// right
	4, 5, 6,
	6, 7, 4,
	// back
	8, 9, 10,
	10, 11, 8,
	// left
	12, 13, 14,
	14, 15, 12,
	// bottom
	16, 17, 18,
	18, 19, 16,
	// top
	20, 21, 22,
	22, 23, 20
];

enum BlitVS = "
#version 450 core

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec2 o_uv;

void main()
{
	gl_Position = vec4(pos, 0, 1);
	o_uv = uv;
}
";

enum BlitFS = "
#version 450 core

layout(location = 0) in vec2 uv;

layout(location = 0) out vec4 o_col;

layout(binding = 0) uniform sampler2D posTex;
layout(binding = 1) uniform sampler2D normalTex;
layout(binding = 2) uniform sampler2D colSpecTex;

void main()
{
	o_col = texture(colSpecTex, uv);
}
"; 

struct QuadVertex
{
	@Attribute(0, Attribute.Type.f32, 2, false)
	vec2 pos;

	@Attribute(1, Attribute.Type.f32, 2, false)
	vec2 uv;
}

static immutable QuadVertex[4] QuadVertices = [
	QuadVertex(vec2(-1, -1), vec2(0, 0)),
	QuadVertex(vec2( 1, -1), vec2(1, 0)),
	QuadVertex(vec2( 1,  1), vec2(1, 1)),
	QuadVertex(vec2(-1,  1), vec2(0, 1)),
];

static immutable uint[6] QuadIndices = [
	0, 1, 2,
	2, 3, 0
];